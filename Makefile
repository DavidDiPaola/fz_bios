# 2022 David DiPaola, licensed CC0 (public domain worldwide)

SRC = main.s
BIN = bios.bin

COFF = $(BIN:.bin=.coff)

PREFIX ?= z80-unknown-coff-

.phony: all
all: $(BIN)

.phony: clean
clean:
	rm -f $(COFF)
	rm -f $(BIN)

.phony: dump
dump: $(COFF)
	$(PREFIX)objdump --disassemble-all $<

.phony: dump_bin
dump_bin: $(BIN)
	$(PREFIX)objdump --target=binary --architecture=z80 --disassemble-all $<

$(COFF): $(SRC)
	$(PREFIX)as -z80 -forbid-undocumented-instructions -g -o $@ $<

$(BIN): $(COFF)
	$(PREFIX)objcopy -O binary $< $@
